package com.Citics.Application.DataAPI;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;


@SpringBootApplication
public class DataApiApplication {

	public static void main(String[] args) {
		
		SpringApplication app = new SpringApplication(DataApiApplication.class);
        app.setDefaultProperties(Collections
          .singletonMap("server.port", "8083"));
        app.run(args);

	}
	
@Component
public class ServerPortCustomizer 
	  implements WebServerFactoryCustomizer<ConfigurableWebServerFactory> {
	  
	    @Override
	    public void customize(ConfigurableWebServerFactory factory) {
	        factory.setPort(8086);
	    }
	}
}
